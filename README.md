# dota2-hero-data-fetcher

Dota2日本語Wiki用に、ヒーローの各種データをpukiwikiの表形式で出力するプログラム。データは英語Wiki(gamepedia)から取得する。gamepediaのから取得したデータをそのままjsonで出力することもできる。

## 必要なもの

- Node.js

## 使い方

- `git clone`でクローンする。以下のコマンドはプロジェクトのルートフォルダーで実行する。
- `npm install`で依存ライブラリをインストールしてから、`node index.js <ヒーロー名>`で表を出力する(ロールの欄は出力されません)。
- `node index.js -f json <ヒーロー名>`でjsonを出力する。

## 例
### 入力
```
node index.js Axe
```
### 出力
```
|CENTER:130|CENTER:130|CENTER:|CENTER:50|CENTER:25|CENTER:25|CENTER:50|c
|>|~&attachref(image/______.png,nolink,256x144);|~ステータス|~Lv1|>|~Inc|~Lv25|
|~|~|~&color(red){''Strength''};|25|>|2.8|92.2|
|~|~|~Agility|20|>|2.2|72.8|
|~|~|~Intelligence|18|>|1.6|56.4|
|~初期攻撃力|49‒53|~HP|>|762.5|>|2270|
|~基礎攻撃速度|1.7|~HP回復|4.25|>|17.19%|63.39%|
|~攻撃射程|150|~魔法耐性|>|26.35%|>|29.23%|
|~弾速|Melee|~Mana|>|291|>|747|
|~攻撃動作|0.5+0.5|~Mana回復|0.9|>|32.4%|101.52%|
|~移動速度|290|~スペルダメージ|>|1.26%|>|3.95%|
|~振向き速度|0.6|~Armor|>|1.2|>|9.65|
|~衝突幅|24|~Att/sec|>|0.71|>|1.02|
|~視野|1800/800|~移動速度増加|>|1%|>|3.64%|
```
### Pukiwikiプレビュー
![pukiwiki](./example.png "pukiwiki例")
