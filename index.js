const axios = require('axios')
const commander = require('commander')
const cheerio = require('cheerio')
const dec = require('decimal.js')

const url = 'https://dota2.gamepedia.com/api.php?action=parse&format=json&page='
const heroNameRegex = /^[a-z]+(_[a-z]+)*$/i
const statGainRegex = /(\d+) *\+ *(\d+(\.\d+)?)/

const attributes = ['strength', 'agility', 'intelligence']
const attributeColors = ['red', 'green', 'blue']
const maxLevel = 25

const variableStats = ['Health', 'Health_regeneration', 'Magic_resistance', 'Mana', 'Mana_regeneration', 'Spell_damage', 'Armor', 'Attack_speed', 'Movement_speed', 'Attack_damage']
const fixedStats = ["Health_regeneration", "Mana_regeneration", "Movement_speed", "Turn_rate", "Vision", "Attack_range", "Projectile_speed", "Attack_animation", "Base_attack_time", "Magic_resistance", "Collision_size"]

const fetchArticle = heroName => {
  return axios(url + heroName, {
    responseType: 'json',
  }).then(res => res.data)
}

const parseHeroData = data => {
  const html = data.parse.text['*']
  const info = {}
  const $ = cheerio.load(html)
  const table = $('.infobox').first()

  const primary = table.find('#primaryAttribute')
  const attr = primary.parent()
  info.primary_attr = primary.index()

  info.attrs = []
  for (let i = 0; i < attributes.length; i++) {
    const baseAndGain = attr
      .children()
      .eq(i + 3)
      .text()
    const m = statGainRegex.exec(baseAndGain)
    if (m == null) throw new Error('属性値の取得に失敗')
    const initial = new dec(m[1])
    const gain = new dec(m[2])
    const final = gain.times(maxLevel - 1).plus(initial)
    info.attrs.push({ initial, gain, final })
  }

  const table1 = table.find('.evenrowsgray').first()
  info.variable = {}
  for (const stat of variableStats) {
    const row = table1.find(`a[href="/${stat}"]`).closest('tr')
    const initial = row.children().eq(2).text().trim()
    const final = row.children().eq(4).text().trim()
    info.variable[stat] = { initial, final }
  }

  const table2 = table.find('.oddrowsgray').first()
  info.fixed = {}
  for (const stat of fixedStats) {
    const row = table2.find(`a[href="/${stat}"]`).closest('tr')
    const value = row.children().eq(1).text().trim()
    info.fixed[stat] = value
  }

  return info
}

const capitalize = s => {
  return s[0].toUpperCase() + s.slice(1)
}

const generateTable = info => {
  const { primary_attr, attrs, fixed, variable } = info
  let result = 
`|CENTER:130|CENTER:130|CENTER:|CENTER:50|CENTER:25|CENTER:25|CENTER:50|c
|>|~&attachref(image/______.png,nolink,256x144);|~ステータス|~Lv1|>|~Inc|~Lv25|
`

  for (let i = 0; i < attributes.length; i++) {
    let label = `${capitalize(attributes[i])}`
    if (i === primary_attr) {
      label = `&color(${attributeColors[i]}){''${label}''};`
    }
    const { initial, gain, final } = attrs[i]
    result += `|~|~|~${label}|${initial}|>|${gain}|${final}|\n`
  }

  result += 
`|~初期攻撃力|${variable.Attack_damage.initial}|~HP|>|${variable.Health.initial}|>|${variable.Health.final}|
|~基礎攻撃速度|${fixed.Base_attack_time}|~HP回復|${fixed.Health_regeneration}|>|${variable.Health_regeneration.initial}|${variable.Health_regeneration.final}|
|~攻撃射程|${fixed.Attack_range}|~魔法耐性|>|${variable.Magic_resistance.initial}|>|${variable.Magic_resistance.final}|
|~弾速|${fixed.Projectile_speed === 'Instant' ? 'Melee' : fixed.Projectile_speed}|~Mana|>|${variable.Mana.initial}|>|${variable.Mana.final}|
|~攻撃動作|${fixed.Attack_animation}|~Mana回復|${fixed.Mana_regeneration}|>|${variable.Mana_regeneration.initial}|${variable.Mana_regeneration.final}|
|~移動速度|${fixed.Movement_speed}|~スペルダメージ|>|${variable.Spell_damage.initial}|>|${variable.Spell_damage.final}|
|~振向き速度|${fixed.Turn_rate}|~Armor|>|${variable.Armor.initial}|>|${variable.Armor.final}|
|~衝突幅|${fixed.Collision_size}|~Att/sec|>|${variable.Attack_speed.initial}|>|${variable.Attack_speed.final}|
|~視野|${fixed.Vision}|~移動速度増加|>|${variable.Movement_speed.initial}|>|${variable.Movement_speed.final}|`

  return result
}

const main = async () => {
  commander
    .usage('[options] <hero name>')
    .option(
      '-f, --format [value]',
      '出力のフォーマット: pukiwiki(デフォルト), json',
      /^(pukiwiki|json)$/
    )
    .parse(process.argv)

  if (commander.args.length !== 1 || !heroNameRegex.test(commander.args[0])) {
    console.error(
      '引数はヒーロー名をアンダースコア(_)区切りで、大文字小文字を正確に、1つだけ指定してください。ヒーロー名一覧: https://dota2.gamepedia.com/Heroes'
    )
    return
  }

  let info
  try {
    info = await fetchArticle(commander.args[0]).then(parseHeroData)
  } catch (error) {
    console.error(
      'エラー: ヒーロー名が間違っているか、gamepediaのページ構造が予期しないものになっています。'
    )
    console.error(error)
    return
  }

  if (commander.format === 'json') {
    console.log(JSON.stringify(info, null, 2))
    return
  }

  try {
    const tableText = generateTable(info)
    console.log(tableText)
  } catch (error) {
    console.error('エラー: 表の生成に失敗しました。')
    console.error(error)
  }
}

main()
